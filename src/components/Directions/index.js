import React from "react";
import MapViewDirections from "react-native-maps-directions";

const Directions = ({ destination, origin, onReady }) => (
  <MapViewDirections
    destination={destination}
    origin={origin}
    onReady={onReady}
    apikey="AIzaSyBJH6h6YAAo_fmGYImraSVrMNinvusWZrg"
    strokeWidth={3}
    strokeColor="#222"
  />
);

export default Directions;