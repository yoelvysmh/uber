import React, { Component } from "react";

import {
  Container,
  TypeTitle,
  TypeDescription,
  TypeImage,
  RequestButton,
  RequestButtonText
} from "./styles";

import uberx from "../../assets/uberx.png";

export default class Details extends Component {
  render() {
    return (
      <Container>
        <TypeTitle>Popular</TypeTitle>
        <TypeDescription>Viajes baratos todos los dias</TypeDescription>
        <TypeImage source={uberx} />
        <TypeTitle>UberX</TypeTitle>
        <TypeDescription>$4,00</TypeDescription>
        <RequestButton onPress={() => {}}>
          <RequestButtonText>SOLICITAR UBER</RequestButtonText>
        </RequestButton>
      </Container>
    );
  }
}
